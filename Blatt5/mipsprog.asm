    .text
    .globl main
main:
    addi   $sp, $sp, -12
    sw     $ra, 8($sp)
    sw     $fp, 4($sp)
    sw     $v0, 0($sp)
    addi   $fp, $sp, 8
    la     $a0, text_1
    jal    routine_1
    la     $a0, text_3
    jal    routine_1
    jal    routine_3
    move   $s0, $v0
    jal    algorithm
    la     $a0, text_4
    jal    routine_1
    move   $a0, $s1
    jal    routine_2
    la     $a0, text_2
    jal    routine_1
    lw     $ra, 8($sp)
    lw     $fp, 4($sp)
    lw     $v0, 0($sp)
    addi   $sp, $sp, 12
    jr     $ra

routine_1:
    li    $v0, 4
    syscall
    jr    $ra

routine_2:
    li    $v0, 1
    syscall
    jr    $ra

routine_3:
    li    $v0, 5
    syscall
    jr    $ra

algorithm:
    addi    $sp, $sp, -4    #neu
    sw      $ra, 0($sp)     #neu
    li      $s1, 0
    blez    $s0, jump_2
    addi    $t0, $s0, -10
    bgtz    $t0, jump_2

jump_1:
    add     $s1, $s0, $s1
    addi    $s0, $s0, -1
    bgtz    $s0, jump_1
    j       jump_3

jump_2:
    la      $a0, text_5
    jal     routine_1
    li      $s1, -1

jump_3:
    lw      $ra, 0($sp)     #neu
    addi    $sp, $sp, 4     #neu
    jr      $ra             #neu

    .data
text_1: .asciiz "Rechnerorganisation Aufgabe:\n"
text_2: .asciiz "\nEnde!\n"
text_3: .asciiz "Geben Sie eine natuerliche Zahl zwischen 1 und 10 ein: "

text_4: .asciiz "Ergebnis = "
text_5: .asciiz "falsche Eingabe!\n"
