     .text
     .globl main
main:
     addi    $sp, $sp, -4   # save space of stack for reg.
     sw      $ra, 0($sp)    # save return address
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, text1     # load address of text1
     syscall
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, inptxt    # load address of inptxt
     syscall
     li      $v0, 5         # system call no. 5: console input
     syscall
     move    $a1, $v0       # move value N into $a1 register

     jal     factorial

     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, end1      # load address of end1
     syscall
     li      $v0, 1         # system call no. 1: print integer
     move    $a0, $a1       # register $a1 has value N
     syscall
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, nline
     syscall
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, end2      # load address of end1
     syscall
     li      $v0, 1         # system call no. 1: print integer
     move    $a0, $v1       # register $v1 has value N!
     syscall
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, nline
     syscall
     lw      $ra, 0($sp)    # restore return address
     addi    $sp, $sp, 4    # restore stack pointer
     jr      $ra            # return to main program

factorial:
     move    $t1, $a1
     li      $v1, 1
loop:     
     beq     $t1, 0, done
     mul     $v1, $v1, $t1
     sub     $t1, $t1, 1
     j       loop				# jump to loop1
done:
     jr $ra

        .data
text1:  .asciiz "\nFakultaetsberechnung\n"
inptxt: .asciiz "Geben Sie eine natuerliche Zahl zwischen 0 und 12 ein: "
end1:   .asciiz "\nN  = "
end2:   .asciiz "N! = "
nline:  .asciiz "\n"
