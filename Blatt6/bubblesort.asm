# Hauptprogramm
	.text
    .globl main
main:
    addi  $sp,  $sp, -4      # save stack space for registers
    sw    $ra,  0($sp)       # save return address
    la    $a0,  eingabe
    jal   printString
    jal   readValues         # from console
    jal   sortValues
    la    $a0,  ausgabe
    jal   printString
    jal   printValues        # to screen
    li    $a0, 10            # print newline
    jal   printChar
    lw    $ra,  0($sp)       # restore return address
    addi  $sp,  $sp, 4       # restore stack pointer
    jr    $ra

readValues:
    addi  $sp,  $sp, -4      # save stack space for registers
    sw    $ra,  0($sp)       # save return address
    la	  $t1, array         # Wir speichern unseren Array im Datensegment. Hat ja keiner verboten
    li    $t0, 10            # Länge des Arrays
readloop:
    beq	  $t0, $zero, readloopexit # !0 mal nen Wert einlesen dann fertig => readloopexit
    jal   readInt            # Nächsten Integerwert einlesen  
    sw	  $v0, 0($t1)        # Integerwert im Arrayspeichern
    addiu $t1, $t1, 4        # Arraypointer weiterschieben
    addiu $t0, $t0, -1       # Counter um 1 reduzieren
    j	  readloop			 # Und nochmal
readloopexit:
    lw    $ra,  0($sp)       # restore return address
    addi  $sp,  $sp, 4       # restore stack pointer
    jr    $ra

sortValues:
    addi  $sp,  $sp, -4      # save stack space for registers
    sw    $ra,  0($sp)       # save return address
    li    $t1, 10            # Länge des Arrays t1 ist die Zählvariable des Outerloops
outloop:                     # Äußerer Loop
    la	  $t0, array         # Array Adresse laden
    li    $t2, 0             # Zählvariable des Innerloops
inloop:                      # Innererloop
    lw    $t3,  0($t0)       # Ersten Bubblewert laden
    lw    $t4,  4($t0)       # Zweiten Bubblewert laden
    ble	  $t3, $t4, skipswap # Schauen ob getauscht wird
    sw    $t3, 4($t0)        # Tauschen
    sw    $t4, 0($t0)
skipswap:
    addiu $t0, $t0 ,4        # Bubble verschieben
    addiu $t2, $t2 ,1        # Innere Zählvariable inkrementieren
    li    $t5, 9             # Macht aus blt ein blti
    blt   $t2, $t5, inloop   # Nochmal falls t2 < t5
    addiu $t1, $t1, -1       # Äußere Zählvariable dekrementieren
    li    $t5, 1             # Macht aus bgt ein bgti
    bgt	  $t1, $t5, outloop  # Nochmal falls t1 > 1
    lw    $ra,  0($sp)       # restore return address
    addi  $sp,  $sp, 4       # restore stack pointer
    jr    $ra

printValues:
    addi  $sp,  $sp, -4      # save stack space for registers
    sw    $ra,  0($sp)       # save return address
    la	  $t1, array         # Arrayadresse aus dem Datensegment laden
    li    $t0, 10            # Länge des Arrays
    j     noleadingcomma     # Beim ersten mal kein Komma, daher überspringen wir diesen Codeteil
printloop:
    li    $a0, ','           # 
    jal   printChar          # Komma printen
noleadingcomma:
    lw	  $a0, 0($t1)        # Wert aus dem Array auslesen
    jal   printInt           # Wert printen
    addiu $t1, $t1, 4        # Arraypointer weiterschieben
    addiu $t0, $t0, -1       # Counter um 1 reduzieren
    bgt	  $t0, $zero, printloop # if counter = 0 then readloopexit
printloopexit:
    lw    $ra,  0($sp)       # restore return address
    addi  $sp,  $sp, 4       # restore stack pointer
    jr    $ra

# Routinen für Ein- und Ausgabe	 
printString:
    li    $v0, 4             # print a string
    syscall
    jr    $ra                # return to calling routine

printInt:
    li    $v0, 1             # print an integer
    syscall
    jr    $ra                # return to calling routine

readInt:
    li    $v0, 5             # read an integer 
    syscall
    jr    $ra    

printChar:
    li    $v0, 11             # print an integer
    syscall
    jr    $ra 

.data
array:      .word   0, 0, 0, 0, 0, 0, 0, 0, 0, 0
eingabe:    .asciiz "Bitte 10 Zahlen eingeben:\n"
ausgabe:    .asciiz "sortierte Ausgabe: "