#include <stdio.h>
#define N 10

void algo(int n,int* v){
    int i,j,m,p;
    for (i = 0;  i < n ; i++){
        v[i] = 0;
    }
    i = 1;
    m = n/2;
    v[m] = 1;
    while(i<n-1){
        p = v[m];
        j = m;
        while(j < n){
            if(i % 2 == 1){
                p += v[j];
                p = p ^ v[j];
                v[j] = p ^ v[j];
                p = p ^ v[j];
            }
            if(j<n-1 && i % 2 == 0){
                v[j] += v[j+1];
            }
            j++;
        }
        i++; 
    }
    for(j = 0; j < m; j++){
        v[j]=v[n-1-j];
    }
}

int main(){
    int n = N;
    int v[N];
    algo(n,v);
    for (int i = 0; i < n; i++){
        printf("%i,",v[i]);
    }
    return 0;
}