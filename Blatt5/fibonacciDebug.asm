    .text
    .globl main
main:
	move  $s2, $sp
    addiu $sp,  $sp, -4      # reserve stack $space for 1 register
    sw    $ra,  0($sp)       # save return address on stack
    move  $fp,  $sp          # set $fp = $sp
    li    $v0,  4            # code 4 == print string
    la    $a0,  text_1       # $a0 == address of the string
    syscall                  # print string
    li    $v0,  5            # set $v0 = index of system call "read_int"
    syscall                  # read integer value n from console
    ble	  $v0,  $zero, error # if $v0 <= $zero then goto error
    move  $a0,  $v0		     # $a0 = $v0
    jal	  fibrek			 # jump to fibrek and save position to $ra
    move  $a0,  $v0          # $a0 = $v0
    li    $v0,  1            # set $v0 = index of system call "print_int"
    syscall                  # print on console: value of returned result
    j	  done				 # jump to done
error:
    li    $v0,  4            # code 4 == print string
    la    $a0,  text_2       # $a0 == address of the string
    syscall                  # print string
done:
    lw    $ra,  0($fp)       # restore return address from stack
    addiu $sp,  $fp, 4       # release reserved stack $space
    jr    $ra                # return to operating system


fibrek:
    #Prolog
    addiu $sp,  $sp, -12     # reserve stack $space for 3 register
    sw    $ra,  0($sp)       # save return address on stack
    sw    $s0,  4($sp)       # save $s0 on stack
    sw    $s1,  8($sp)       # save $s1 on stack
    
	#Debug
	move $t1, $s2
	move $s3, $a0
    li    $v0,  4            # code 4 == print string
    la    $a0,  text_3       # $a0 == address of the string
    syscall                  # print string
debug:
	li $v0, 1
	lw $a0, 0($t1)
	syscall
    li    $v0,  11            # code 4 == print string
    la    $a0,  10       # $a0 == address of the string
    syscall                  # print string
	addi $t1, $t1, -4
	bge $t1, $sp, debug
	move $a0, $s3
	
    #Algorithm
    li	  $v0,  1		     # precautious result = 1
    li    $t0,  2
    ble	  $a0,  $t0, fibrek_done	 # if $a0 <= $t0 then goto fibrek_done

    addiu $s0,  $a0, -1      # argument for fibrek(n-1)
    addu  $s1,  $a0, -2      # argument for fibrek(n-2)

    move  $a0,  $s0          # move value into argument register
    jal   fibrek             # call fibrek
    move  $s0,  $v0          # move result into saved register

    move  $a0,  $s1          # move value into argument register
    jal   fibrek             # call fibrek
    move  $s1,  $v0          # move result into saved register
    
    addu   $v0,  $s0, $s1    #add results

fibrek_done:
    #Epilog
    lw    $s1,  8($sp)       # restore $s1 from stack
    lw    $s0,  4($sp)       # restore $s0 from stack
    lw    $ra,  0($sp)       # restore return address from stack
    addiu $sp,  $sp, 12      # release reserved stack $space
    jr    $ra                # return to operating system

    .data
text_1: .asciiz "Zahl eingeben:\n"
text_2: .asciiz "Ungültige Eingabe\n"
text_3: .asciiz "Stack:\n"
